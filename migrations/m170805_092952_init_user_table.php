<?php

use yii\db\Migration;

class m170805_092952_init_user_table extends Migration
{

    public function up()
    {
        $this->createTable('user', [
            'id' => $this->primaryKey(),
            'username' => $this->string(),
            'password' => $this->string(),
            'authKey' => $this->string(),
			'firstName' => $this->string(),
			'lastName' => $this->string(),
			'email' => $this->string(),
			'address'=>$this->string(),
			'created_at' => $this->date(),
		]);
   }

    public function down()
    {
        echo "m170805_092952_init_user_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
